<img src="./terraform.io.png">

<p style="text-align: right; font-size: small;">
by Ikar Pohorský
<br>
https://gitlab.com/ikar/ctvrtkon-terraform
</p>

---
# terraform
- nástroj od HashiCorp (vagrant)
- popis infrastruktury kódem
- jako AWS CloudFormation, ale více
- multiplatformní (linux, osx, win)
- open-source

---
### popis infrastruktury kódem
- nemusíte klikat ;)
- kód jako dokumentace
- verzování
- jednodušší "deprovisioning"

Notes:
- code copy&paste vs click copy&paste

---
## podporované platformy
- AWS
- Google Cloud Platform
- Microsoft Asure
- DigitalOcean
- Gitlab, GitHub
- Docker, Kubernetes
- a asi ~30 dalších "providerů"

---
### příklad SSH security group v AWS
<!-- .slide: style="text-align: left;"> -->
struktura:
```bash
$ tree -a
.
├── ikar.gitignore.auto.tfvars
├── main.tf
├── provider.tf
└── variables.tf
```

---
## instalace terraform
- stáhnout terraform z:
https://www.terraform.io/downloads.html

Notes:
- tutorial (vim included)

---
## konfigurace AWS providera
```bash
$ cat provider.tf
provider "aws" {
	region     = "${var.region}"
	access_key = "${var.access_key}"
	secret_key = "${var.secret_key}"
}
```

---
## definice proměnných
```bash
$ cat variables.tf
variable "region" {
	/*
	   TODO:
	   change to closer datacenter one day
	 */
	default = "eu-central-1"  # Frankfurt
}
```

---
### definice proměnných (.gitignore)
```bash
$ cat ikar.gitignore.auto.tfvars
access_key = "AKIA1232130972435533"
secret_key = "sdklfjhdsfkgjdfglksjdgldksjhgdslkfjdslfk"
```

---
#### terraform init
<img src="./init.png">

---
## security group@AWS
```
$ cat main.tf
resource "aws_security_group" "my_ssh_public" {
	name = "my-ssh-public"
	description = "Allow SSH from anywhere"
	ingress = {
		protocol = "TCP"
		from_port = 22
		to_port = 22
		cidr_blocks = ["0.0.0.0/0"]
	}
}
```

---
## terraform plan
<img src="./plan.png">

---
#### terraform apply
<img src="./apply.png">

---
### příklad SSH security group v AWS
<img src="./aws-sg.png">

---
### příklad SSH security group v AWS
<!-- .slide: style="text-align: left;"> -->
struktura:
```bash
$ tree -a
.
├── ikar.gitignore.auto.tfvars
├── main.tf
├── provider.tf
├── .terraform
│   └── plugins
│       └── linux_amd64
│           ├── lock.json
│           └── terraform-provider-aws_v2.4.0_x4
├── terraform.tfstate
└── variables.tf
```

---
## plán a stav
<img src="./tfstate-apply.png">

---
## sdílení stavu
- local (`terraform.tfstate`)
- remote (dříve: terraform enterprise)
- S3, HTTP, PG, ...
- locking

---
## datové centrum v čr?
```diff
diff --git infra/variables.tf infra/variables.tf
index 9da17af..78dcb16 100644
--- infra/variables.tf
+++ infra/variables.tf
@@ -3,5 +3,5 @@ variable "secret_key" {}

 variable "region" {
-       default = "eu-central-1"
+       default = "eu-east-1"
 }

```

---
# shrnutí
- infrastruktura jako kód
- multiplatformní
- dobrá dokumentace, tutorial
- komunita

---
# dotazy?

<p style="text-align: right; font-size: small;">
by Ikar Pohorský
<br>
https://gitlab.com/ikar/ctvrtkon-terraform
</p>
