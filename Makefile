# print help
help: ## list available targets (this page)
	@awk 'BEGIN {FS = ":.*?## "} /^[0-9a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

reveal: install run  ## reveal-md presentation

install: .yarn.success  ## install all deps from package.json
	@echo "All deps satisfied!"

.yarn.success: package.json
	@yarn install && touch .yarn.success

run:
	node_modules/.bin/reveal-md \
		--host 0.0.0.0 \
		--watch \
		--theme white \
		--port 2019 \
		README.md

pdf: ctvrtkon.pdf
	@echo "Done ✓"

ctvrtkon.pdf: README.md
	node_modules/.bin/reveal-md \
		--theme white \
		--print ctvrtkon.pdf \
		README.md

clean:  ## clean all the rubbish
	rm -rf ./node_modules
	rm yarn.lock .yarn.success
